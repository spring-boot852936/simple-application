package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        return args -> {
            Student riadh = new Student(
                    "riadh",
                    LocalDate.of(1993, Month.MAY, 04),
                    "riadh@gmail.com"
            );
            Student lilou = new Student(
                    "Lilou",
                    LocalDate.of(2021, Month.MAY, 19),
                    "lilou@gmail.com"
            );

            studentRepository.saveAllAndFlush(List.of(riadh, lilou));
        };
    }
}
